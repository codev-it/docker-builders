# Codev-IT
# docker env builder
#

# default
NAME="Codev-IT"
EMAIL="office@codev-it.at"

# docker info
DOCKER=docker
DOCKER_ID=codevit
DOCKER_TAG=latest
DOCKER_FILE=Dockerfile

.PHONY: help build test push shell run start stop logs clean release

help:
	@echo "Please use \`make <target>' where <target> is one of"
	@echo "  build	     		build docker image"
	@echo "  build-all	   		build all folders"
	@echo "  push		   		push to docker hub"
	@echo "  push-all	   		push all to docker hub"
	@echo "  shell    	   		start bash session"
	@echo "  run		   		run container"
	@echo "  - CMD=val    		docker container command"
	@echo "  detach		   		run container in detach mode"
	@echo "  detach-all	   		run all container in detach mode"
	@echo "  remove		   		remove container"
	@echo "  remove-all	   		remove all container"
	@echo "  logs		   		get logs"
	@echo "  logs-all	   		get all logs"

build:
	@$(DOCKER) build \
		-t "$$(echo "${DOCKER_ID}/${DOCKER_NAME}:${DOCKER_TAG}" \
			| tr '[:upper:]' '[:lower:]')" \
			--build-arg BUILD_DATE=`date -u +"%Y-%m-%dT%H:%M:%SZ"` \
			--build-arg VCS_REF=`git rev-parse --short HEAD` \
			--build-arg VERSION="${DOCKER_TAG}" \
			"${PWD}";

build-all:
	@for build in ${PWD}/*; do \
		if [ -f "$$build/Dockerfile" ]; then \
			make build DOCKER_NAME="$${build##*/}" PWD="$${PWD}/$${build##*/}"; \
		fi \
	done

push:
	@$(DOCKER) push ${DOCKER_ID}/${DOCKER_NAME}:${DOCKER_TAG}

push-all:
	@for build in ${PWD}/*; do \
		if [ -f "$$build/Dockerfile" ]; then \
			make push DOCKER_NAME="$${build##*/}"; \
		fi \
	done

shell:
	@$(DOCKER) run --rm --name ${DOCKER_NAME} -i -t $(OPTS) $(PORTS) $(DNS) $(VOLUMES) $(ENV) ${DOCKER_ID}/${DOCKER_NAME}:${DOCKER_TAG} ${DOCKER_SHELL}

run:
	@$(DOCKER) run --rm --name ${DOCKER_NAME} -i -t -e DEBUG=1 $(OPTS) $(PORTS) $(DNS) $(VOLUMES) $(ENV) ${DOCKER_ID}/${DOCKER_NAME}:${DOCKER_TAG} $(CMD)

detach:
	@$(DOCKER) run -id --rm --name ${DOCKER_NAME} $(PARMS) $(OPTS) $(DNS) $(VOLUMES) $(ENV) ${DOCKER_ID}/${DOCKER_NAME}:${DOCKER_TAG} $(CMD)

detach-all:
	@for build in ${PWD}/*; do \
		if [ -f "$$build/Dockerfile" ]; then \
			make detach DOCKER_NAME="$${build##*/}"; \
		fi \
	done

stop:
	@$(DOCKER) stop ${DOCKER_NAME}

exec:
	@$(DOCKER) exec -i ${DOCKER_NAME} $(CMD)

exec-all:
	@for build in ${PWD}/*; do \
		if [ -f "$$build/Dockerfile" ]; then \
			make exec DOCKER_NAME="$${build##*/}"; \
		fi \
	done

logs:
	@$(DOCKER) logs $(PARMS) ${DOCKER_NAME}

logs-all:
	@for build in ${PWD}/*; do \
		if [ -f "$$build/Dockerfile" ]; then \
			make logs DOCKER_NAME="$${build##*/}"; \
		fi \
	done

remove:
	@$(DOCKER) rm -f ${DOCKER_NAME}

remove-all:
	@for build in ${PWD}/*; do \
		if [ -f "$$build/Dockerfile" ]; then \
			make remove DOCKER_NAME="$${build##*/}"; \
		fi \
	done

release: build push

bitbucket-pip:
	@$(DOCKER) run --rm --name ${DOCKER_NAME} -i -t --workdir="/localDebugRepo" --memory=4g --memory-swap=4g --memory-swappiness=0 --entrypoint=/bin/bash $(OPTS) $(PORTS) $(DNS) $(VOLUMES) $(ENV) ${DOCKER_ID}/${DOCKER_NAME}:${DOCKER_TAG} $(CMD)