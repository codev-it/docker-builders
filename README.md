# Docker Builders #

Docker Build Templates

### debian-stretch-clean ###

Default debian build.

**Packages**
 * git
 * curl
 * nano
 * bash-completion

### debian-build_essential-clean ###

Debian build with build essentials.

**Packages**
 * git
 * curl
 * nano
 * cmake
 * quilt
 * lintian
 * dh-make
 * fakeroot
 * debhelper
 * zlib1g-dev
 * ninja-build
 * build-essential
 * bash-completion
 
### debian-build_essential-ruby ###

Debian Ruby build with build essentials.

**Packages**
 * git
 * curl
 * nano
 * cmake
 * quilt
 * lintian
 * dh-make
 * fakeroot
 * debhelper
 * zlib1g-dev
 * ninja-build
 * build-essential
 * bash-completion
 * ruby-full
 * ruby-json
 * zlib1g-dev
 * bundler
 
### debian-build_essential-python ###

Debian Python build with build essentials.

**Packages**
 * git
 * curl
 * nano
 * cmake
 * quilt
 * lintian
 * dh-make
 * fakeroot
 * debhelper
 * zlib1g-dev
 * ninja-build
 * build-essential
 * bash-completion
 * python-dev
 * python3-all
 * python3-setuptools
 * python3-pip
 * python-stdeb