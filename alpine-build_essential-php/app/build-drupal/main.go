package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"regexp"
	"strings"
	"sync"
)

func main() {
	// get base path
	pwd, err := os.Getwd()
	assertFatal(err)

	// parse arguments
	name := flag.String("name", getName(), "Set the test app name.")
	webRoot := flag.String("root", "/var/www/html", "Set the web root path.")
	typ := flag.String("type", "", "Type to test: profile, module, theme or library. (default: '')")
	composer := flag.String("composer", "install", "Composer command.")
	contrib := flag.Bool("contrib", false, "Enable contribute test path.")
	presetDir := flag.String("preset-dir", "/presets", "Drupal composer preset directory.")
	presetName := flag.String("preset", "drupal-composer.json", "Drupal composer preset from the preset directory.")
	testPath := flag.String("test-dir", "", "Test path relative to the module.")
	runTests := flag.Bool("run-phpunit", false, "Run phpUnit tests.")
	report := flag.Bool("report", false, "Create phpUnit junit report.")
	noCleanup := flag.Bool("no-cleanup", false, "Do not clean test folder before clean.")
	d9Poly := flag.Bool("drupal-9-poly", false, "remove the drupal core package from test composer.")
	flag.Parse()

	// set Preset
	preset := fmt.Sprintf("%s/%s", *presetDir, *presetName)

	// check phpUnit origin file
	xmlFile := fmt.Sprintf("%s/phpunit.xml.dist", pwd)
	xmlFileOrig := fmt.Sprintf("%s/phpunit.xml.dist.orig", pwd)
	if _, err := os.Open(xmlFileOrig); err == nil {
		assertFatal(file(xmlFileOrig, xmlFile))
	}

	// copy files
	typS := *typ
	dest := *webRoot
	if typS != "" {
		if typS[len(typS)-1:] != "s" {
			typS = fmt.Sprintf("%ss", typS)
		}
		if !*contrib {
			dest = fmt.Sprintf("%s/web/%s/%s", *webRoot, typS, *name)
		} else {
			dest = fmt.Sprintf("%s/web/%s/contrib/%s", *webRoot, typS, *name)
		}
	}
	if !*noCleanup {
		assertFatal(cleanDir(*webRoot))
	}
	assertFatal(dir(pwd, dest))

	// composer
	if typS != "" {
		createComposerDepends(*presetDir, preset, *webRoot, *d9Poly)
	}
	if *composer != "skip" {
		execCmd(*webRoot, "composer", *composer)
	}

	// // run tests
	phpUnitSrc := fmt.Sprintf("%s/phpunit.xml.dist", pwd)
	phpUnitDst := fmt.Sprintf("%s/phpunit.xml.dist", *webRoot)
	phpUnitTest := fmt.Sprintf("%s/%s", dest, *testPath)
	if typS == "" && *testPath == "" {
		phpUnitTest = ""
	}
	assertFatal(file(phpUnitSrc, phpUnitDst))
	if *runTests {
		if !*report {
			execCmd(*webRoot, "phpunit",
				"--debug", "--stop-on-failure", phpUnitTest)
		} else {
			reportFile := fmt.Sprintf("%s/report.xml", pwd)
			execCmd(*webRoot, "phpunit",
				"--debug", "--stop-on-failure", "--log-junit", reportFile, phpUnitTest)
		}
	}
}

func getName() string {
	files, err := ioutil.ReadDir(".")
	assertFatal(err)
	for _, f := range files {
		re := regexp.MustCompile(`(?m)\.info\.yml`)
		match := re.FindAllString(f.Name(), -1)
		if len(match) == 1 {
			return strings.Split(f.Name(), ".")[0]
		}
	}
	return ""
}

func createComposerDepends(presetPath string, presetFilePath string, rootPath string, d9Poly bool) {
	// load json files
	var composer map[string]interface{}
	var preset map[string]interface{}
	composerFile := "composer.json"
	_, err := os.Stat(composerFile)
	if err == nil {
		jsonFile, err := os.Open(composerFile)
		assertPanic(err)
		defer func() {
			err := jsonFile.Close()
			assertFatal(err)
		}()
		byteValue, _ := ioutil.ReadAll(jsonFile)
		assertFatal(json.Unmarshal(byteValue, &composer))
	}
	presetFile, err := os.Open(presetFilePath)
	assertPanic(err)
	defer func() {
		err := presetFile.Close()
		assertFatal(err)
	}()
	byteValue, _ := ioutil.ReadAll(presetFile)
	assertFatal(json.Unmarshal(byteValue, &preset))

	// remove drupal repository from preset if exist
	presetN := make(map[string]interface{})
	if sliceKeyExist(composer, "repositories") {
		for k, v := range preset {
			re := regexp.MustCompile(`(?m)https://packages\.drupal\.org/8`)
			match := re.FindAllString(fmt.Sprintf("%v", v), -1)
			if len(match) == 0 {
				presetN[k] = v
			}
		}
	}
	if len(presetN) != 0 {
		preset = presetN
	}

	if d9Poly {
		if sliceKeyExist(composer, "require") {
			require := make(map[string]string)
			for k, v := range composer {
				if k == "require" {
					if i, ok := v.(map[string]interface{}); ok {
						for key, val := range i {
							if key != "drupal/core" {
								require[key] = val.(string)
							}
						}
					}
				}
			}

			if len(require) != 0 {
				composer["require"] = require
			}
		}
	}

	// marge files
	var result interface{}
	result, err = merge(composer, preset)
	assertPanic(err)

	// create new composer file
	file, _ := json.MarshalIndent(result, "", "  ")
	newComposer := fmt.Sprintf("%s/composer.json", rootPath)
	assertFatal(ioutil.WriteFile(newComposer, file, 0644))
	rootScripts := fmt.Sprintf("%s/scripts", rootPath)
	assertFatal(dir(fmt.Sprintf("%s/scripts", presetPath), rootScripts))
	assertFatal(dir(fmt.Sprintf("%s/root", presetPath), rootPath))
}

func assertFatal(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func assertFatalF(err error, format string) {
	if err != nil {
		log.Fatalf(format, err)
	}
}

func assertPanic(err error) {
	if err != nil {
		log.Panic(err)
	}
}

func sliceKeyExist(list map[string]interface{}, a string) bool {
	for b := range list {
		if b == a {
			return true
		}
	}
	return false
}

func merge(x1, x2 interface{}) (interface{}, error) {
	data1, err := json.Marshal(x1)
	if err != nil {
		return nil, err
	}
	data2, err := json.Marshal(x2)
	if err != nil {
		return nil, err
	}
	var j1 interface{}
	err = json.Unmarshal(data1, &j1)
	if err != nil {
		return nil, err
	}
	var j2 interface{}
	err = json.Unmarshal(data2, &j2)
	if err != nil {
		return nil, err
	}
	return merge1(j1, j2), nil
}

func merge1(x1, x2 interface{}) interface{} {
	switch x1 := x1.(type) {
	case map[string]interface{}:
		x2, ok := x2.(map[string]interface{})
		if !ok {
			return x1
		}
		for k, v2 := range x2 {
			if v1, ok := x1[k]; ok {
				x1[k] = merge1(v1, v2)
			} else {
				x1[k] = v2
			}
		}
	case []interface{}:
		x2, ok := x2.([]interface{})
		if !ok {
			return x1
		}
		for i := range x2 {
			x1 = append(x1, x2[i])
		}
		return x1
	case nil:
		// merge(nil, map[string]interface{...}) -> map[string]interface{...}
		x2, ok := x2.(map[string]interface{})
		if ok {
			return x2
		}
	}
	return x1
}

func file(src, dst string) error {
	var err error
	var srcfd *os.File
	var dstfd *os.File
	var srcinfo os.FileInfo

	if srcfd, err = os.Open(src); err != nil {
		return err
	}
	defer func() {
		err := srcfd.Close()
		assertFatal(err)
	}()

	if dstfd, err = os.Create(dst); err != nil {
		return err
	}
	defer func() {
		err := dstfd.Close()
		assertFatal(err)
	}()

	if _, err = io.Copy(dstfd, srcfd); err != nil {
		return err
	}
	if srcinfo, err = os.Stat(src); err != nil {
		return err
	}
	return os.Chmod(dst, srcinfo.Mode())
}

func dir(src string, dst string) error {
	var err error
	var fds []os.FileInfo
	var srcinfo os.FileInfo

	if srcinfo, err = os.Stat(src); err != nil {
		return err
	}

	if err = os.MkdirAll(dst, srcinfo.Mode()); err != nil {
		return err
	}

	if fds, err = ioutil.ReadDir(src); err != nil {
		return err
	}
	for _, fd := range fds {
		srcfp := path.Join(src, fd.Name())
		dstfp := path.Join(dst, fd.Name())

		if fd.IsDir() {
			if err = dir(srcfp, dstfp); err != nil {
				fmt.Println(err)
			}
		} else {
			if err = file(srcfp, dstfp); err != nil {
				fmt.Println(err)
			}
		}
	}
	return nil
}

func cleanDir(dir string) error {
	d, err := os.Open(dir)
	if err != nil {
		assertFatal(os.MkdirAll(dir, 0755))
	}
	defer func() {
		err := d.Close()
		assertFatal(err)
	}()
	names, err := d.Readdirnames(-1)
	assertFatal(err)
	for _, name := range names {
		assertFatal(os.RemoveAll(filepath.Join(dir, name)))
	}
	return nil
}

func execCmd(dir string, name string, args ...string) {
	cmd := exec.Command(name, args...)
	cmd.Dir = dir

	var stdoutBuf, stderrBuf bytes.Buffer
	stdoutIn, _ := cmd.StdoutPipe()
	stderrIn, _ := cmd.StderrPipe()

	var errStdout, errStderr error
	stdout := io.MultiWriter(os.Stdout, &stdoutBuf)
	stderr := io.MultiWriter(os.Stderr, &stderrBuf)
	err := cmd.Start()
	assertFatalF(err, "Exec failed with '%s'\n")

	var wg sync.WaitGroup
	wg.Add(1)

	go func() {
		_, errStdout = io.Copy(stdout, stdoutIn)
		wg.Done()
	}()

	_, errStderr = io.Copy(stderr, stderrIn)
	wg.Wait()

	err = cmd.Wait()
	assertFatalF(err, "Exec failed with '%s'\n")
	if errStdout != nil || errStderr != nil {
		log.Fatal("failed to capture stdout or stderr\n")
	}
}
