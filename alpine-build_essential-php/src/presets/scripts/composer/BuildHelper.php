<?php

/**
 * @file
 * Contains \DrupalProject\composer\ScriptHandler.
 */

namespace DrupalProject\composer;

use Composer\Script\Event;
use DrupalFinder\DrupalFinder;

/**
 * Class BuildHelper.
 *
 * @package      DrupalProject\composer
 *
 * @noinspection PhpUnused
 */
class BuildHelper {

  /**
   * Set www redirect to htaccess.
   *
   * @param \Composer\Script\Event $event
   *
   * @noinspection PhpUnused
   */
  public static function wwwRedirect(Event $event) {
    $drupalFinder = new DrupalFinder();
    $drupalFinder->locateRoot(getcwd());
    $drupalRoot = $drupalFinder->getDrupalRoot();

    $domain = !empty($event->getArguments()[0])
      ? sprintf('%s/', $event->getArguments()[0]) : "%{HTTP_HOST}";
    $htaccess_path = sprintf('%s/.htaccess', $drupalRoot);
    $htaccess_str = file_get_contents($htaccess_path);

    $frist_con = "RewriteCond %{HTTP_HOST} .";
    $second_con = "RewriteCond %{HTTP_HOST} !^www\. [NC]";
    $third_con = "RewriteRule ^ http%{ENV:protossl}://www.%{HTTP_HOST}%{REQUEST_URI} [L,R=301]";
    $htaccess_str = str_replace(sprintf('# %s', $frist_con), $frist_con, $htaccess_str);
    $htaccess_str = str_replace(sprintf('# %s', $second_con), $second_con, $htaccess_str);
    if (empty($domain)) {
      $htaccess_str = str_replace(sprintf('# %s', $third_con), $third_con, $htaccess_str);
    }
    else {
      $third_rpl = "RewriteRule ^ http%{ENV:protossl}://www." . $domain . "%{REQUEST_URI} [L,R=301]";
      $htaccess_str = str_replace(sprintf('# %s', $third_con), $third_rpl, $htaccess_str);
    }

    file_put_contents($htaccess_path, $htaccess_str);
  }

  /**
   * Disable robots.
   *
   * @noinspection PhpUnused
   */
  public static function disableRobots() {
    $drupalFinder = new DrupalFinder();
    $drupalFinder->locateRoot(getcwd());
    $drupalRoot = $drupalFinder->getDrupalRoot();
    $robots_path = sprintf('%s/robots.txt', $drupalRoot);
    $robots_str = "User-agent: *\nDisallow: /\n";
    file_put_contents($robots_path, $robots_str);
  }

}
