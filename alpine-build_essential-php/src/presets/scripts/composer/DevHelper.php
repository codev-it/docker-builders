<?php

/**
 * @file
 * Contains \DrupalProject\composer\ScriptHandler.
 */

namespace DrupalProject\composer;

use Composer\Script\Event;
use DrupalFinder\DrupalFinder;

/**
 * Class DevHelper.
 *
 * @package      DrupalProject\composer
 *
 * @noinspection PhpUnused
 */
class DevHelper {

  /**
   * Check translation files.
   *
   * @param \Composer\Script\Event $event
   *
   * @noinspection PhpUnused
   */
  public static function checkTranslationsFile(Event $event) {
    $drupalFinder = new DrupalFinder();
    $drupalFinder->locateRoot(getcwd());
    $drupalRoot = $drupalFinder->getDrupalRoot();

    $type = !empty($event->getArguments()[0]) ? $event->getArguments()[0] : '';
    $name = !empty($event->getArguments()[1]) ? $event->getArguments()[1] : '';
    $langcode = !empty($event->getArguments()[2]) ? $event->getArguments()[2] : 'de';

    if (!empty($type) && !empty($name)) {
      $type = $type[strlen($type) - 1] !== 's' ? sprintf('%ss', $type) : $type;
      $type_path = sprintf('%s/%s', $drupalRoot, $type);
      if (file_exists($type_path)) {
        $path = static::getFolderDir($type_path, $name);
        $translation_file = sprintf('%s/translations/%s.po', $path, $langcode);
        $code_translations = static::getTranslationsDeclarationsFromCode($path);
        $config_translations = static::getTranslationsDeclarationsFromConfig($path);
        $tpl_translations = static::getTranslationsDeclarationsFromTpl($path);
        $combined_translations = array_replace(
          $code_translations, $config_translations, $tpl_translations);
        $translation_file_translations = static::getPoFileContent($translation_file);

        $event->getIO()->write('================================');
        $event->getIO()->write('This strings are not translated:');
        $event->getIO()->write('================================');
        foreach ($combined_translations as $translation) {
          if (empty($translation_file_translations[$translation])) {
            $write = sprintf('==> %s', $translation);
            $event->getIO()->write($write);
          }
        }

        $event->getIO()->write('================================');
        $event->getIO()->write('This translated are not used:');
        $event->getIO()->write('================================');
        foreach ($translation_file_translations as $key => $translation) {
          if (!in_array($key, $combined_translations)) {
            $write = sprintf('==> %s', $key);
            $event->getIO()->write($write);
          }
        }

      }
    }
  }

  /**
   * Get the folder to search for.
   *
   * @param $path
   * @param $dir
   *
   * @return string
   */
  private static function getFolderDir($path, $dir) {
    if (file_exists($path) && is_dir($path)) {
      $file_name = sprintf('%s.info.yml', $dir);
      $it = new \RecursiveDirectoryIterator($path);
      /** @var \SplFileInfo $file */
      foreach (new \RecursiveIteratorIterator($it) as $file) {
        if ($file->getFilename() == $file_name) {
          return $file->getPath();
        }
      }
    }
    return $path;
  }

  /**
   * Get string set by translation function.
   *
   * @param $path
   *
   * @return array
   */
  private static function getTranslationsDeclarationsFromCode($path) {
    $ret = [];
    if (file_exists($path) && is_dir($path)) {
      $it = new \RecursiveDirectoryIterator($path);
      /** @var \SplFileInfo $file */
      foreach (new \RecursiveIteratorIterator($it) as $file) {
        if ($file->getFilename()[0] !== '.'
          && is_file($file->getPathname())) {
          $re = '/\Wt\([\'|"](.*)[\'|"][| ]*[)|,]|@[t|T]ranslation\([\'|"](.*)[\'|"]\)/';
          $file_content = file_get_contents($file->getPathname());
          preg_match_all($re, $file_content, $matches, PREG_SET_ORDER, 0);
          if (!empty($matches)) {
            foreach ($matches as $match) {
              $str = trim(end($match));
              if (!empty($str)) {
                $ret[$str] = $str;
              }
            }
          }
        }
      }
    }
    return $ret;
  }

  /**
   * Get from yml files.
   *
   * @param $path
   *
   * @return array
   */
  private static function getTranslationsDeclarationsFromConfig($path) {
    $ret = [];
    if (file_exists($path) && is_dir($path)) {
      $it = new \RecursiveDirectoryIterator($path);
      /** @var \SplFileInfo $file */
      foreach (new \RecursiveIteratorIterator($it) as $file) {
        if ($file->getFilename()[0] !== '.'
          && is_file($file->getPathname())) {
          $re = '/\.schema\.yml/m';
          $suffix = explode('.', $file->getFilename());
          $suffix = end($suffix);
          preg_match_all($re, $file->getFilename(), $matches, PREG_SET_ORDER, 0);
          if (empty($matches) && $suffix == 'yml') {
            $re = '/title[\s]*:[\s]*(.*)|label[\s]*:[\s]*(.*)|description[\s]*:[\s]*(.*)/m';
            $file_content = file_get_contents($file->getPathname());
            preg_match_all($re, $file_content, $matches, PREG_SET_ORDER, 0);
            if (!empty($matches)) {
              foreach ($matches as $match) {
                if (!empty($match)) {
                  $str = trim(end($match));
                  $first = str_replace('\'', '"', $str[0]);
                  $last = str_replace('\'', '"', $str[strlen($str) - 1]);
                  if ($first == '"' && $last == '"') {
                    $max = strlen($str) - 2;
                    $str = strlen($str) > 2 ? substr($str, 1, $max) : '';
                  }
                  if (!empty($str)) {
                    $ret[$str] = $str;
                  }
                }
              }
            }
          }
        }
      }
    }
    return $ret;
  }

  /**
   * Get translation from templates.
   *
   * @param $path
   *
   * @return array
   */
  private static function getTranslationsDeclarationsFromTpl($path) {
    $ret = [];
    if (file_exists($path) && is_dir($path)) {
      $it = new \RecursiveDirectoryIterator($path);
      /** @var \SplFileInfo $file */
      foreach (new \RecursiveIteratorIterator($it) as $file) {
        if ($file->getFilename()[0] !== '.'
          && is_file($file->getPathname())) {
          $e = explode('.', $file->getFilename());
          $extension = end($e);
          if ($extension == 'twig') {
            $re = '/[\'|"](.*)[\'|"]\|t[\W]/m';
            $file_content = file_get_contents($file->getPathname());
            preg_match_all($re, $file_content, $matches, PREG_SET_ORDER, 0);
            if (!empty($matches)) {
              foreach ($matches as $match) {
                $str = trim(end($match));
                if (!empty($str)) {
                  $ret[$str] = $str;
                }
              }
            }
          }
        }
      }
    }
    return $ret;
  }

  /**
   * Get po file content.
   *
   * @param $file
   *
   * @return array
   */
  private static function getPoFileContent($file) {
    $ret = [];
    if (file_exists($file)) {
      $current = NULL;
      $translations = [];
      foreach (file($file) as $line) {
        if (substr($line, 0, 5) == 'msgid') {
          $current = trim(substr(trim(substr($line, 5)), 1, -1));
        }
        if (substr($line, 0, 6) == 'msgstr') {
          $translations[$current] = trim(substr(trim(substr($line, 6)), 1, -1));
        }
      }

      foreach ($translations as $msgid => $msgstr) {
        $msgid = str_replace('\"', '"', $msgid);
        $ret[$msgid] = $msgstr;
      }
    }
    return $ret;
  }

}
